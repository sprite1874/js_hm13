const url = "https://jsonplaceholder.typicode.com/comments";
const req = new XMLHttpRequest();
const modalLoader = document.querySelector(".modal-loader");
 // функція запиту на сервер
function jsonSend () {
    modalLoader.classList.add("active");
    req.open("GET", url);
    req.send();
    req.addEventListener("readystatechange", () => {
        if(req.readyState === 4 && req.status >= 200 && req.status < 300){
            showComm(JSON.parse(req.responseText));
            modalLoader.classList.remove("active");
        }else if(req.readyState === 4){
            throw new Error("Помилка у запиті!");
        }
    })
}

//document.getElementById("button1").addEventListener("click", jsonSend)
//document.getElementById("button2").addEventListener("click", jsonSend)

// оголошення змінної для виводу інфи
let number = 1

// перебір массиву з кнопками та запис значення у змінну "намбер"
const [...button] = document.querySelectorAll("button")
button.forEach((el)=>{
    el.addEventListener("click", (el)=>{
        if(el.target.innerText == "PUSH 1") {
          number = 1
        }
         else if(el.target.innerText == "PUSH 2") {
          number = 2
        }
         else if(el.target.innerText == "PUSH 3") {
          number = 3
        }
         else if(el.target.innerText == "PUSH 4") {
          number = 4
        }
         else if(el.target.innerText == "PUSH 5") {
          number = 5
        }
         else if(el.target.innerText == "PUSH 6") {
          number = 6
        }
         else if(el.target.innerText == "PUSH 7") {
          number = 7
        }
         else if(el.target.innerText == "PUSH 8") {
          number = 8
        }
         else if(el.target.innerText == "PUSH 9") {
          number = 9
        }
         else if(el.target.innerText == "PUSH 10") {
          number = 10
        }
    })
})


// навішуємо подію "клік" на кнопки
document.querySelectorAll("button").forEach(elem => {
  elem.addEventListener("click", jsonSend)
})




const divv = document.getElementById("1");
const div23 = document.getElementById("23");


// функція створення коментаря з перебором і вибором того , що потрібен нам
function showComm (data) {
   //  console.log(data)
    divv.innerHTML = ""
  //  console.log(typeof "data")
    if(!Array.isArray(data)){
        throw new Error("З сервера прийшов не масив!");
    }
   if (number == 1) {
    data.filter(obj=> obj.postId == 1).forEach((obj)=>{
       // console.log(obj)
        div23.prepend(createComm(obj)) 
             
    })}
    if (number ==2) {
    data.filter(obj=> obj.postId == 2).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==3) {
    data.filter(obj=> obj.postId == 3).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==4) {
    data.filter(obj=> obj.postId == 4).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==5) {
    data.filter(obj=> obj.postId == 5).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==6) {
    data.filter(obj=> obj.postId == 6).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==7) {
    data.filter(obj=> obj.postId == 7).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==8) {
    data.filter(obj=> obj.postId == 8).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==9) {
    data.filter(obj=> obj.postId == 9).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
      if (number ==10) {
    data.filter(obj=> obj.postId == 10).forEach((obj)=>{
       // console.log(obj)
         div23.prepend(createComm(obj)) 
            // document.body.prepend(createComm(obj))         
    })}
}

// функція "вставки" хтмл коду коментарів у сторінку

function createComm ({postId, id, name, email, body}) {

    divv.insertAdjacentHTML("beforeend", ` <div class="borderComm"> 
    <div class="name"><span class="bold font-weight-bold">Імя:</span> ${name}</div>
    <div class="comm"><span class="bold font-weight-bold">Коментар:</span> ${body}</div>     
    <div class="idComm"><span class="bold font-weight-bold">ID:</span> ${id}</div>
     <div class="email"><span class="bold font-weight-bold">Email:</span> ${email}</div> <div> `);

    return divv;
//  }
}

// виклик функції щоб відображався перший коментар при завантаженні сторінки
jsonSend()
//showComm(data)